/**
 * Aristotle - shared/mc
 * Copyright(c) 2015 Bolt, LLC.
 */

const QUESTION_RE = new RegExp("^(\\d+(?:\\.|\\))\\s*)(.*)");
const GROUP_RE = new RegExp("(\\(?\\w+(?:\\.|\\))\\s*)(.*)");

class MultipleChoiceParser {
	isQuestion(s) {
		return QUESTION_RE.exec(s);
	}

	parse(text) {
		var lines = text.split("\n").map(e => e.trim());
		var questions = [];
		var curr = undefined;
		var line = undefined;
		var nextId = 1;

		for (var i in lines) {
			line = lines[i];
			if (line) {
				var match = GROUP_RE.exec(line);
				if (match && match.length > 2 && match[2]) {
					if (this.isQuestion(line)) {
						if (curr) questions.push(curr);
						curr = {'text': match[2], 'answers': []};
						nextId = 1;
					} else {
						if (curr) {
							curr['answers'].push({ id: nextId, text: match[2] });
							nextId++;
						}
					}
				}
			}
		}

		if (curr) questions.push(curr);

		return questions;
	}
}

export default new MultipleChoiceParser();
