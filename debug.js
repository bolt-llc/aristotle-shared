
/**
 * Logs out the current stack trace
 */
export function logStack() {
	try {
		throw new Error();
	} catch (err) {
		console.log(err.stack);
	}
}