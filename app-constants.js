export const CONTENT_TYPES = [
	{ value: 'FR', icon: 'pencil', 			label: 'Free Response' }, // autograded: no
	{ value: 'MC', icon: 'list', 			label: 'Multiple Choice' }, // autograded: yes
	{ value: 'B',  icon: 'check-square-o', 	label: 'True/False' }, // autograded: yes
	{ value: 'MD', icon: 'file-text-o', 	label: 'Text' }, // autograded: no
	{ value: 'E',  icon: 'code', 			label: 'Embed' }, // autograded: no
	{ value: 'U',  icon: 'upload', 			label: 'Upload File' } // autograded: no
];

export const CLASSWORK_TYPES = [
	{ value: 'L', label: 'Lesson' },
	{ value: 'E', label: 'Assessment' },
	{ value: 'A', label: 'Assignment' }
];

export const DAY_MAP = {
	'M': { index: 0, full: 'Monday' },
	'T': { index: 1, full: 'Tuesday' },
	'W': { index: 2, full: 'Wednesday' },
	'R': { index: 3, full: 'Thursday' },
	'F': { index: 4, full: 'Friday' },
	'S': { index: 5, full: 'Saturday' },
	'U': { index: 6, full: 'Sunday' }
};

export const ORGANIZATION_ROLES = [
	'general', 
	'instructor', 
	'admin', 
	'owner'
];

export const SECTION_ROLES = [
	'parent',
	'student',
	'ta', 
	'instructor'
];

export const ATTENDANCE_STATUSES = [
	{ value: 'P', label: 'Present' },
	{ value: 'L', label: 'Late' },
	{ value: 'A', label: 'Absent' },
	{ value: 'E', label: 'Excused' },
	{ value: 'U',  label: 'Unknown' }
]

export const STATUSES = {
	'pending': 'Pending',
	'inactive': 'Inactive',
	'active': 'Active'
};

export const URL_VALIDATOR_OPTS = {
	protocols: [ 'http', 'https' ]
}

export const IMAGE_TYPES = [
	'image/bmp',
	'image/png',
	'image/jpeg',
	'image/tiff',
	'image/gif'	
];